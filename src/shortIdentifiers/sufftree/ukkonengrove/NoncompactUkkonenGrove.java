package shortIdentifiers.sufftree.ukkonengrove;


import shortIdentifiers.general.Hello;
import shortIdentifiers.general.SpecCharacter;
import shortIdentifiers.general.tree.AlphabetTree;

public interface NoncompactUkkonenGrove<Type extends
	NoncompactUkkonenGroveAttributes<Type> &
	AlphabetTree<Type>>
extends
	Hello<Type>{
	
	public default Type newTree(){
		
		getMe().setLeadingLeaf(getMe());
		
		return getMe();
	}
	
	public default Type grow(SpecCharacter c){
		
		/*
		 * It's actually the same as original Ukkonen
		 * This implementation requires:
		 * - good semantics of addSon (see Alphabet Tree)
		 * - buffer ref-node over root (with son method pointing back to root)
		 */
		
		
		Type newLeaf = getMe().getLeadingLeaf().addSon(c);
		Type toGetSon = getMe().getLeadingLeaf().getRefLink();
		getMe().setLeadingLeaf(newLeaf); //store new head
		Type toGetRef = newLeaf;
		Type limesSon = toGetSon.getSon(c);
		while(limesSon == null){
			newLeaf = toGetSon.addSon(c);
			toGetRef.setRefLink(newLeaf);
			toGetSon = toGetSon.getRefLink();
			toGetRef = newLeaf;
			limesSon = toGetSon.getSon(c);
		}
		toGetRef.setRefLink(limesSon);
		
		return getMe();
	}

}
