package shortIdentifiers.sufftree.ukkonengrove;

public interface NoncompactUkkonenGroveAttributes<Type> {
	
	public Type getRefLink();
	public void setRefLink(Type tree);
	
	public Type getLeadingLeaf();
	public void setLeadingLeaf(Type tree);

}
