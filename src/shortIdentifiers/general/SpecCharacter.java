package shortIdentifiers.general;

public class SpecCharacter {
	
	Character c;
	int i;
	
	public SpecCharacter(Character c){
		this.c = c;
	}
	
	public SpecCharacter(char c){
		this.c = new Character(c);
	}
	
	public SpecCharacter(int i){
		this.i = i;
		c = null;
	}
	
	public boolean isSpecial(){
		return c == null;
	}
	
	public Character toChar(){
		return c;
	}
	
	public int getNumber(){
		return i;
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof SpecCharacter){
			if(c == null) return i == ((SpecCharacter) o).getNumber();
			else return c.equals(((SpecCharacter) o).toChar());
		}
		else return false;
	}
	
	@Override
	public int hashCode(){
		if(c == null) return i;
		else return c.hashCode();
				
	}

}
