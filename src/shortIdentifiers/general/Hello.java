package shortIdentifiers.general;

public interface Hello<Type> {
	
	public Type getMe();

}
