package shortIdentifiers.general;

public interface Visitor<Type> {
	
	public void visit(Type element);

}
