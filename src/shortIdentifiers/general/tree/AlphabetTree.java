package shortIdentifiers.general.tree;

import shortIdentifiers.general.SpecCharacter;

public interface AlphabetTree<Type> {
	
	Type getSon(SpecCharacter c);
	
	Type addSon(SpecCharacter c);
	/*
	 * Required semantics:
	 * When son doesn't exists return created son.
	 * When it does, do nothing and return him instead.
	 */
	

}
