package shortIdentifiers.general.tree;

import java.util.Collection;
import java.util.LinkedList;

import shortIdentifiers.general.Visitor;

public interface BFSTree<MyType extends DirectedGraph<MyType>> {
	
	public Collection<MyType> getSons();
	public MyType getMe();
	
	public default void runBFS(Visitor<MyType> visitor){
		
		MyType ptr;
		LinkedList<MyType> fifo = new LinkedList<MyType>();
		fifo.add(getMe());
		
		while(! fifo.isEmpty()){
			ptr = fifo.poll();
			visitor.visit(ptr);
			for(MyType son : ptr.getSons()){
				fifo.add(son);
			}
		}
	}

}
