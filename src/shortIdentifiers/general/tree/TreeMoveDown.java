package shortIdentifiers.general.tree;

import shortIdentifiers.general.Hello;

public interface TreeMoveDown<Type extends
		DirectedGraph<Type> & TreeMoveDown<Type>>
extends
		Hello<Type>{
	
	public default Type moveDown(){
		
		Type ptr = getMe();
		while(! ptr.getSons().isEmpty())
			ptr = ptr.getSons().iterator().next();
		
		return ptr;
	}
}
