package shortIdentifiers.general.tree;

import java.util.Collection;

public interface DirectedGraph<T> {
	
	public Collection<T> getSons();

}
