package shortIdentifiers.general.tree;

import shortIdentifiers.general.Hello;

public interface AttachedNode<Type extends AttachedNode<Type>>
extends
	Hello<Type>{
	
	public boolean isRoot();
	
	public Type getFather();
	
	public default Type moveUp(){
		Type ptr = getMe();
		while(! ptr.isRoot())
			ptr = ptr.getFather();
		return ptr;
	}

}
