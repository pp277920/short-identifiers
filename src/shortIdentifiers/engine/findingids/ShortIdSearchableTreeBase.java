package shortIdentifiers.engine.findingids;

public interface ShortIdSearchableTreeBase {
	
	public String getWord();
	public int getLeafCount();
	public int getIdn();
	public boolean isSpecialNode();

}
