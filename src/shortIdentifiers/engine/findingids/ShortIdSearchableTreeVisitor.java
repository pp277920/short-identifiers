package shortIdentifiers.engine.findingids;

import java.util.Map;

import shortIdentifiers.general.Visitor;
import shortIdentifiers.general.tree.AttachedNode;
import shortIdentifiers.general.tree.BFSTree;
import shortIdentifiers.general.tree.DirectedGraph;
import shortIdentifiers.general.tree.TreeMoveDown;

public class ShortIdSearchableTreeVisitor<Type extends
	ShortIdSearchableTreeBase &
	DirectedGraph<Type> &
	TreeMoveDown<Type> &
	BFSTree<Type> &
	AttachedNode<Type> &
	ShortIdSearchableTree<Type>> 
implements 
		Visitor<Type> {
	
	/*
	 * Default visitor.
	 */
	
	private Map<Integer, String> map;
	
	public ShortIdSearchableTreeVisitor(Map<Integer, String> map) {
		this.map = map;
	}

	@Override
	public void visit(Type node) {
		
		//debug
		System.out.print(node.isRoot());
		System.out.print(node.isSpecialNode());
		System.out.print(node.getLeafCount());
		if(! node.isSpecialNode()) System.out.print(node.getWord());
		System.out.println();
		
		//we assume noncompact version
		
		int idn;
		if(node.getLeafCount() == 1 && ! node.isSpecialNode()) {
			idn = node.moveDown().getIdn();
			if(map.get(idn) == null) {
				map.put(idn, node.getWord());
			}
		}
	}

}
