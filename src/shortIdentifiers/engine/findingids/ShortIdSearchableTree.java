package shortIdentifiers.engine.findingids;

import java.util.HashMap;

import shortIdentifiers.general.Hello;
import shortIdentifiers.general.tree.AttachedNode;
import shortIdentifiers.general.tree.BFSTree;
import shortIdentifiers.general.tree.DirectedGraph;
import shortIdentifiers.general.tree.TreeMoveDown;

public interface ShortIdSearchableTree<Type extends
	ShortIdSearchableTree<Type> &
	ShortIdSearchableTreeBase &
	DirectedGraph<Type> &
	AttachedNode<Type> &
	TreeMoveDown<Type> &
	BFSTree<Type> >
extends
	Hello<Type>{
	
	public default HashMap<Integer, String> findShortIds(){
		
		HashMap<Integer, String> ans = new HashMap<Integer, String>();
		
		getMe().runBFS(new ShortIdSearchableTreeVisitor<Type>(ans));
		
		return ans;
	}

}