package shortIdentifiers.engine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.LinkedList;

import shortIdentifiers.general.SpecCharacter;

public class ShortIdWorkerRoot extends ShortIdWorker {
	
	/*
	 * Main class for exterior usage.
	 * It adds shortIds implementation, which
	 * executes algorithm.
	 */
	
	private boolean track;
	private Collection<Collection<ShortIdWorker>> leavesFamily;
	private Collection<ShortIdWorker> leaves;
	
	public ShortIdWorkerRoot(){
		super();
		root = this;
		ref = new ShortIdRootBuffer(this);
	}
	
	/*
	 * Tracking nodes stores references to newly
	 * created nodes in the tree.
	 * In ShortIds we track leaves to perform counting from them.
	 */
	
	public boolean isTracking(){
		return track;
	}
	
	public void trackNode(ShortIdWorker n){
		leaves.add(n);
	}
	
	/*
	 * Main methods.
	 * For input of identifiers in lines (or list)
	 * a map from line (or index) to shortest
	 * identifier is created.
	 */
	
	public Map<Integer, String> shortIds(String input){
		String[] lines = input.split("\\r?\\n");
		ArrayList<String> list = new ArrayList<String>(Arrays.asList(lines));
		return shortIds(list);
	}
		
		
	public Map<Integer, String> shortIds(List<String> input){
		
		leavesFamily = new ArrayList<Collection<ShortIdWorker>>(input.size());
		int i = 0;
		for(String line : input){
			++i;
			track = false;
			this.newTree();
			for(char c : line.toCharArray()){
				System.out.print(c);
				this.grow(new SpecCharacter(c));
			}
			System.out.println();
			track = true;
			leaves = new LinkedList<ShortIdWorker>();
			this.grow(new SpecCharacter(i));
			leavesFamily.add(leaves);
		}
		
		this.doCounting(leavesFamily);
		
		return this.findShortIds();
	}
	
	@Override
	public boolean isSpecialNode(){
		return true;
	}

}