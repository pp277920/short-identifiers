package shortIdentifiers.engine;

import shortIdentifiers.general.SpecCharacter;

public class ShortIdRootBuffer extends ShortIdWorker {
	
	/*
	 * Node of the word of length -1
	 */
	
	public ShortIdRootBuffer(ShortIdWorkerRoot root){
		super();
		this.root = root; 
	}
	
	@Override
	public ShortIdWorker getSon(SpecCharacter c){
		return root;
	}

}
