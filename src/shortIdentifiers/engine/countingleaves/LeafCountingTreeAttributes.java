package shortIdentifiers.engine.countingleaves;

public interface LeafCountingTreeAttributes {
	
	public void incLeafCount();
	
	public boolean isRoot();
		
	public int getMark();
	public void setMark(int paint);

}
