package shortIdentifiers.engine.countingleaves;

import java.util.Collection;

import shortIdentifiers.general.tree.AttachedNode;

public interface LeafCountingTree<Type extends LeafCountingTree<Type> & AttachedNode<Type>> extends
		LeafCountingTreeAttributes {
	
	/*
	 * Counts and stores the number of how many different
	 * (special) characters are there in the leaves below given node.
	 * 
	 * Input: Family of sets of pointers to leaves corresponding
	 * to some idn.
	 */
	
	public default void doCounting(Collection<Collection<Type>> family){
		
		/*
		 * For each set in family we mark each node above nodes
		 * from the set with corresponding paint.
		 * The number of different paints is updated.
		 */
		
		int paint = 0;
		Type ptr;
		for(Collection<Type> set : family){
			++paint;
			for(Type leaf : set){
				ptr = leaf;
				System.out.print("Malujemy: ");
				System.out.println(paint);
				while(! (ptr.getMark() == paint) && ! (ptr.isRoot())){
					ptr.incLeafCount();
					ptr.setMark(paint);
					ptr = ptr.getFather();
				}
			}
		}
	}

}
