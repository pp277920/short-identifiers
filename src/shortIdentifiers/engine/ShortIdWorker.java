package shortIdentifiers.engine;

import java.util.Collection;
import java.util.HashMap;

import shortIdentifiers.engine.countingleaves.LeafCountingTree;
import shortIdentifiers.engine.findingids.ShortIdSearchableTree;
import shortIdentifiers.engine.findingids.ShortIdSearchableTreeBase;
import shortIdentifiers.general.SpecCharacter;
import shortIdentifiers.general.tree.AlphabetTree;
import shortIdentifiers.general.tree.AttachedNode;
import shortIdentifiers.general.tree.BFSTree;
import shortIdentifiers.general.tree.DirectedGraph;
import shortIdentifiers.general.tree.TreeMoveDown;
import shortIdentifiers.sufftree.ukkonengrove.NoncompactUkkonenGrove;
import shortIdentifiers.sufftree.ukkonengrove.NoncompactUkkonenGroveAttributes;

/*
 * This is class for nodes required for
 * main algorithm.
 * Running algorithms is done from Root. 
 */

public class ShortIdWorker implements
	AlphabetTree<ShortIdWorker>,
	AttachedNode<ShortIdWorker>,
	BFSTree<ShortIdWorker>,
	DirectedGraph<ShortIdWorker>,
	TreeMoveDown<ShortIdWorker>,
	NoncompactUkkonenGroveAttributes<ShortIdWorker>,
	NoncompactUkkonenGrove<ShortIdWorker>,
	LeafCountingTree<ShortIdWorker>,
	ShortIdSearchableTreeBase,
	ShortIdSearchableTree<ShortIdWorker>
{
	
	/*
	 * Attributes
	 */
	
	private ShortIdWorker father;
	protected ShortIdWorker ref;
	private ShortIdWorker leading;
	protected ShortIdWorkerRoot root;
	private int mark, count;
	private String word;
	private SpecCharacter tag;
	private HashMap<SpecCharacter, ShortIdWorker> sons;
	
	
	/*
	 * Methods
	 */
	
	public ShortIdWorker(){
		/*
		 * Partial constructor for Root
		 */
		father = null;
		mark = 0;
		count = 0;
		word = "";
		sons = new HashMap<SpecCharacter, ShortIdWorker>();
	}
	
	public ShortIdWorker(ShortIdWorker father, SpecCharacter c){
		this.father = father;
		root = father.root;
		mark = 0;
		count = 0;
		tag = c;
		sons = new HashMap<SpecCharacter, ShortIdWorker>();
		if(! c.isSpecial()) word = father.getWord().concat(c.toChar().toString());
	}
	
	/*
	 * Required Methods
	 */

	@Override
	public ShortIdWorker getMe() {
		return this;
	}

	@Override
	public ShortIdWorker getRefLink() {
		return ref;
	}

	@Override
	public void setRefLink(ShortIdWorker tree) {
		ref = tree;
	}

	@Override
	public ShortIdWorker getLeadingLeaf() {
		return leading;
	}

	@Override
	public void setLeadingLeaf(ShortIdWorker tree) {
		leading = tree;
	}

	@Override
	public void incLeafCount() {
		++count;
	}

	@Override
	public boolean isRoot() {
		return father == null;
	}

	@Override
	public int getMark() {
		return mark;
	}

	@Override
	public void setMark(int paint) {
		mark = paint;
	}

	@Override
	public ShortIdWorker getFather() {
		return father;
	}

	@Override
	public String getWord() {
		return word;
	}

	@Override
	public int getLeafCount() {
		return count;
	}

	@Override
	public int getIdn() {
		return tag.getNumber();
	}

	@Override
	public Collection<ShortIdWorker> getSons() {
		return sons.values();
	}

	@Override
	public ShortIdWorker getSon(SpecCharacter c) {
		return sons.get(c);
	}

	@Override
	public ShortIdWorker addSon(SpecCharacter c) {
		
		if(sons.get(c) != null) return sons.get(c);
		//nothing to add
		
		ShortIdWorker ans = new ShortIdWorker(this, c);
		sons.put(c, ans);
		if(root.isTracking()) root.trackNode(ans);
		return ans;
	}

	@Override
	public boolean isSpecialNode() {
		return tag.isSpecial();
	}

}
