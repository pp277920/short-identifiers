package shortIdentifiers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import shortIdentifiers.engine.ShortIdWorkerRoot;

public class ShortIdentifiers {
	
	/*
	 * Simple program using ShortIdentifiers library
	 * 2 arguments are required
	 * Input file path with identifiers
	 * Output file path
	 */
	
	public static void main(String[] args){
		
		if(args.length < 2) return;
		
		Path input_p = Paths.get(args[0]);
		Path output_p = Paths.get(args[1]);
		
		List<String> lines;
		try {
			lines = Files.readAllLines(input_p);
			ShortIdWorkerRoot tree = new ShortIdWorkerRoot();
			Map<Integer, String> ans = tree.shortIds(lines);
			List<String> output = new ArrayList<String>(lines.size());
			for(int i = 1; i <= lines.size(); i++){
				output.add(ans.get(i));
			}
			
			if(! Files.exists(output_p)) Files.createFile(output_p);
			Files.write(output_p, output);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
