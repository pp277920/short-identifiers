# Short Identifiers README #

* Version 1.0

### About ###

Short Identifiers is a small project enabling one functionality:
Given list of identifiers (strings) we "trim" them to (contiguous) minimal ones such that
they still uniquely represent them among original ones (i.e. string matching will point the right original identifier).

### How to use ###

Class ShortIdentifiers compiles to a program doing the task given two path files.
Given txt file with identifiers split in lines analogous file with short identifiers is created.

### License and Guarantee  ###

Feel free to use this code in any way.
No guarantee is given.